sublime-czar-theme
=================

A customized version of the sublime-vim-theme, with an emphasis on providing more unique colors

Specail thanks to [@aziz](https://github.com/aziz/tmTheme-Editor) for providing much of the boiler-plate as well as an easy color picker.

Installation
------------

Run `./install.sh`

